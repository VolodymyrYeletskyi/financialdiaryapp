package com.example.financial

import android.app.Application
import com.example.financial.util.viewmodel.ViewModelFactory

class Application : Application() {

    val viewModelFactory = ViewModelFactory()
}
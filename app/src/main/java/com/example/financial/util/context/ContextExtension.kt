package com.example.financial.util.context

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.example.financial.Application

fun Context.getViewModelProvider(viewModelStoreOwner: ViewModelStoreOwner): ViewModelProvider {
    val viewModelFactory = (applicationContext as Application).viewModelFactory
    return ViewModelProvider(viewModelStoreOwner, viewModelFactory)
}
package com.example.financial.util.enum

enum class TransactionType {
    ALL,
    INCOME,
    EXPENSE
}
package com.example.financial.util.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.financial.data.repository.TransactionRepository
import com.example.financial.data.repository.mock.MockUserRepository
import com.example.financial.data.repository.UserRepository
import com.example.financial.data.repository.mock.MockTransactionRepository
import com.example.financial.ui.authorization.SignUpViewModel
import com.example.financial.ui.authorization.AuthorizationViewModel
import com.example.financial.ui.splash.SplashViewModel
import com.example.financial.ui.transaction.list.TransactionListViewModel
import com.example.financial.ui.transaction.new_transaction.NewTransactionViewModel
import java.lang.IllegalStateException

class ViewModelFactory : ViewModelProvider.Factory {

    private val userRepository: UserRepository by lazy {
        MockUserRepository()
    }

    private val transactionRepository: TransactionRepository by lazy {
        MockTransactionRepository()
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when(modelClass) {
            SplashViewModel::class.java  -> SplashViewModel(userRepository)
            AuthorizationViewModel::class.java  -> AuthorizationViewModel(userRepository)
            SignUpViewModel::class.java  -> SignUpViewModel(userRepository)
            TransactionListViewModel::class.java  -> TransactionListViewModel(
                transactionRepository)
            NewTransactionViewModel::class.java -> NewTransactionViewModel(transactionRepository)
            else ->  throw IllegalStateException("IllegalStateException")
        } as T
    }
}
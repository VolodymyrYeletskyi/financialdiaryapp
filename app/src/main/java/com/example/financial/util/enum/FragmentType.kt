package com.example.financial.util.enum

enum class FragmentType {
    TAB,
    NEW,
    DETAILS
}
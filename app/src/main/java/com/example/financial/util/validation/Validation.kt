package com.example.financial.util.validation

sealed class Validation

object NewTransaction : Validation() {

    suspend fun validateTransactionName(name: String): Error {
        val regex = Regex("^[a-zA-Z0-9]+\$")
        if (regex.matchEntire(name) == null) {
            return Error.WRONG_NAME
        }
        return Error.SUCCESS
    }

    suspend fun validateTransactionAmount(amount: String): Error {
        val regex = Regex("^[0-9]+\$")
        if (regex.matchEntire(amount) == null) {
            return Error.WRONG_AMOUNT
        }
        return Error.SUCCESS
    }

    enum class Error {
        WRONG_NAME,
        WRONG_AMOUNT,
        SUCCESS
    }
}

object Password : Validation() {

    suspend fun validatePassword(password: String): Error {
        if(password.length !in 4..20) {
            return Error.INVALID_PASSWORD
        }
        return Error.SUCCESS
    }

    enum class Error {
        SUCCESS,
        WRONG_PASSWORD,
        INVALID_PASSWORD
    }
}

object Login : Validation() {

    suspend fun validateLogin(login: String): Error {
        if (login.length !in 1..20) {
            return Error.NAME_LENGTH
        }
        val regex = Regex("^[a-zA-Z0-9]+\$")
        if (regex.matchEntire(login) == null) {
            return Error.NAME_REGEX
        }
        return Error.SUCCESS
    }

    enum class Error {
        SUCCESS,
        WRONG_NAME,
        NAME_LENGTH,
        NAME_REGEX
    }
}

package com.example.financial.util.exception

import java.lang.Exception

class BadCredentialsException(message: String) : Exception(message)
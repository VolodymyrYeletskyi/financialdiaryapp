package com.example.financial.data.entity

data class Credentials(val name: String, val password: String)
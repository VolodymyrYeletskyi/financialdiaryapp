package com.example.financial.data.repository.mock

import android.util.Log
import com.example.financial.data.entity.Transaction
import com.example.financial.data.repository.TransactionRepository
import com.example.financial.util.enum.TransactionType
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.time.LocalDate

class MockTransactionRepository : TransactionRepository {

    private val transactions = mutableListOf(
        Transaction("Salary", TransactionType.INCOME, 2000, "My salary"),
        Transaction("Shopping", TransactionType.EXPENSE, 1500, "Bought new car"),
        Transaction("Lottery", TransactionType.INCOME, 10000, "Won in the lottery")
    )

    private val allStateFlow= MutableStateFlow(filterTransactions(TransactionType.ALL))
    private val incomeStateFlow = MutableStateFlow(filterTransactions(TransactionType.INCOME))
    private val expenseStateFlow= MutableStateFlow(filterTransactions(TransactionType.EXPENSE))

    override suspend fun getTransactions(transactionType: TransactionType): StateFlow<List<Transaction>> {
        return when(transactionType) {
            TransactionType.ALL -> allStateFlow
            TransactionType.INCOME -> incomeStateFlow
            TransactionType.EXPENSE -> expenseStateFlow
        }
    }

    override suspend fun removeTransaction(transaction: Transaction) {
        transactions.remove(transaction)
        fillStateFlow(transaction)
    }

    override suspend fun addTransaction(transaction: Transaction) {
        delay(2000)
        transactions.add(transaction)
        fillStateFlow(transaction)
    }

    private fun filterTransactions(transactionType: TransactionType): List<Transaction> {
        return if (transactionType == TransactionType.ALL) {
            transactions.toMutableList()
        } else {
            transactions.filter { it.type == transactionType }.toMutableList()
        }
    }

    private fun fillStateFlow(transaction: Transaction) {
        allStateFlow.value = filterTransactions(TransactionType.ALL)
        if (transaction.type == TransactionType.INCOME) {
            incomeStateFlow.value = filterTransactions(TransactionType.INCOME)
        } else {
            expenseStateFlow.value = filterTransactions(TransactionType.EXPENSE)
        }
    }
}
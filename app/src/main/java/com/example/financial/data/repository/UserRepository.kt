package com.example.financial.data.repository

import com.example.financial.data.entity.Credentials

interface UserRepository {

    suspend fun isAuthorized(): Boolean

    suspend fun signIn(credential: Credentials): Result<Unit>
}
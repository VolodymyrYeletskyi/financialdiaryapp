package com.example.financial.data.repository.mock

import com.example.financial.data.entity.Credentials
import com.example.financial.data.repository.UserRepository
import com.example.financial.util.exception.BadCredentialsException
import kotlinx.coroutines.delay

class MockUserRepository : UserRepository {

    private val credentials = mutableListOf(
        Credentials("Vasya", "1234")
    )

    private var authorized = false

    override suspend fun isAuthorized() = authorized

    override suspend fun signIn(credential: Credentials): Result<Unit> {
        delay(2000)
        if(credential.name != credentials.last().name) {
            return Result.failure(BadCredentialsException("Wrong username"))
        }
        if(credential.password != credentials.last().password) {
            return Result.failure(BadCredentialsException("Wrong password"))
        }
        if(credential == credentials.last()) {
            authorized = true
            return Result.success(Unit)
        }
        return Result.failure(BadCredentialsException("Repository does not work"))
    }
}
package com.example.financial.data.entity

import com.example.financial.util.enum.TransactionType
import java.time.LocalDate
import java.util.*

data class Transaction (
    val name: String,
    val type: TransactionType,
    val amount: Int,
    val description: String
)
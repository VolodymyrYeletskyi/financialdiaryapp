package com.example.financial.data.repository

import com.example.financial.data.entity.Transaction
import com.example.financial.util.enum.TransactionType
import kotlinx.coroutines.flow.Flow

interface TransactionRepository {

    suspend fun getTransactions(transactionType: TransactionType): Flow<List<Transaction>>

    suspend fun removeTransaction(transaction: Transaction)

    suspend fun addTransaction(transaction: Transaction)
}
package com.example.financial.ui.transaction.new_transaction

import androidx.lifecycle.LiveData
import com.example.financial.data.entity.Transaction
import com.example.financial.data.repository.TransactionRepository
import com.example.financial.ui.base.BaseViewModel
import com.example.financial.util.enum.TransactionType
import com.example.financial.util.livedata.SingleLiveEvent
import com.example.financial.util.validation.NewTransaction
import kotlinx.coroutines.launch

class NewTransactionViewModel(
    private val transactionRepository: TransactionRepository
) : BaseViewModel() {

    private val _rightTransactionEvent = SingleLiveEvent<Unit>()
    val rightTransactionEvent: LiveData<Unit> = _rightTransactionEvent

    private val _wrongTransactionEvent = SingleLiveEvent<NewTransaction.Error>()
    val wrongTransactionEvent: LiveData<NewTransaction.Error> = _wrongTransactionEvent

    fun validateTransaction(
        name: String,
        type: TransactionType,
        amount: String,
        description: String
    ) = scope.launch{
        showProgressBar()
        val invalidName = NewTransaction.validateTransactionName(name)
        val invalidAmount = NewTransaction.validateTransactionAmount(amount)

        if (invalidName == NewTransaction.Error.SUCCESS &&
            invalidAmount == NewTransaction.Error.SUCCESS) {
            _rightTransactionEvent.postValue(Unit)
            addTransaction(Transaction(
                name,
                type,
                amount.toInt(),
                description
            ))
        }
        else {
            if (invalidName == NewTransaction.Error.WRONG_NAME) {
                _wrongTransactionEvent.postValue(NewTransaction.Error.WRONG_NAME)
            }
            if (invalidAmount == NewTransaction.Error.WRONG_AMOUNT) {
                _wrongTransactionEvent.postValue(NewTransaction.Error.WRONG_AMOUNT)
            }
        }
        hideProgressBar()
    }

    private fun addTransaction(transaction: Transaction) = scope.launch {
        transactionRepository.addTransaction(transaction)
    }
}
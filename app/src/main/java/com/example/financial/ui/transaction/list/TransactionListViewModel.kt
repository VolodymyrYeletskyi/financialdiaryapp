package com.example.financial.ui.transaction.list

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.financial.data.entity.Transaction
import com.example.financial.data.repository.TransactionRepository
import com.example.financial.ui.base.BaseViewModel
import com.example.financial.util.enum.TransactionType
import com.example.financial.util.livedata.SingleLiveEvent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch

class TransactionListViewModel(
    private val transactionRepository: TransactionRepository
) : BaseViewModel() {

    private val _getTransactionsEvent = SingleLiveEvent<Collection<Transaction>>()
    val getTransactionsEvent: LiveData<Collection<Transaction>> = _getTransactionsEvent

    fun removeTransaction(transaction: Transaction) = scope.launch{
        transactionRepository.removeTransaction(transaction)
    }

    fun getTransactions(transactionType: TransactionType) = scope.launch{
        transactionRepository.getTransactions(transactionType).collect { value ->
            _getTransactionsEvent.postValue(value) }
    }
}
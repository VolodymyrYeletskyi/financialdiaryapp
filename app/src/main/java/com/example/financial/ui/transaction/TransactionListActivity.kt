package com.example.financial.ui.transaction

import android.os.Bundle
import android.os.PersistableBundle
import com.example.financial.R
import com.example.financial.ui.base.BaseActivity
import com.example.financial.ui.transaction.list.TabFragment
import com.example.financial.ui.transaction.list.TransactionListViewModel
import com.example.financial.ui.transaction.new_transaction.NewTransactionFragment
import com.example.financial.util.enum.FragmentType
import kotlinx.android.synthetic.main.activity_transaction_list.*

class TransactionListActivity : BaseActivity<TransactionListViewModel>(
    R.layout.activity_transaction_list
) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(savedInstanceState != null) {
            fragmentType = savedInstanceState.getSerializable("type") as FragmentType
        }

        showFragment(fragmentType)
        floatingActionButton.setOnClickListener {
            addTransaction()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable("type", fragmentType)
    }

    private fun showFragment(fragmentType: FragmentType) {
        when(fragmentType) {
            FragmentType.TAB -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment, TabFragment.newInstance())
                    .commit()
            }
            FragmentType.NEW -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment, NewTransactionFragment.newInstance())
                    .commit()
            }
            FragmentType.DETAILS ->
                TODO("Add details fragment")
        }
    }

    private fun addTransaction() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment, NewTransactionFragment.newInstance())
            .addToBackStack(null)
            .commit()
    }

    companion object {

        private var fragmentType = FragmentType.TAB
    }
}
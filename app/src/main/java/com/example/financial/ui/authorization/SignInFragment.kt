package com.example.financial.ui.authorization

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.VISIBLE
import com.example.financial.R
import com.example.financial.data.entity.Credentials
import com.example.financial.ui.base.BaseFragment
import com.example.financial.ui.transaction.TransactionListActivity
import com.example.financial.util.livedata.observe
import com.example.financial.util.validation.Login
import com.example.financial.util.validation.Password
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_auth.*

class SignInFragment : BaseFragment<AuthorizationViewModel>(R.layout.fragment_auth) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null) {
            usernameTextInputLayout.editText?.setText(savedInstanceState.getString("name"))
            passwordTextInputLayout.editText?.setText(savedInstanceState.getString("password"))
        }

        setupTextInputLayout(usernameTextInputLayout)
        setupTextInputLayout(passwordTextInputLayout)
        signButton.setOnClickListener {
            checkCredentials()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("name", usernameTextInputLayout.editText?.text.toString())
        outState.putString("password", passwordTextInputLayout.editText?.text.toString())
    }


    private fun setupTextInputLayout(textInputLayout: TextInputLayout?) {
        textInputLayout?.editText?.onFocusChangeListener =
            View.OnFocusChangeListener {_, hasFocus ->  if(hasFocus) textInputLayout?.error = ""}
    }

    override fun subscribe() {
        super.subscribe()
        viewModel.validationSuccessfulEvent.observe(this) {
            hideAllErrors()
        }
        viewModel.userAuthorizedEvent.observe(this) {
            startTransactionListActivity()
        }
        viewModel.invalidLoginEvent.observe(this) {
            showInvalidUsernameMessage(it)
        }
        viewModel.invalidPasswordEvent.observe(this) {
            showInvalidPasswordMessage(it)
        }
    }

    private fun showInvalidUsernameMessage(loginError: Login.Error) {
        hideAllErrors()
        when(loginError) {
            Login.Error.NAME_LENGTH -> {
                usernameTextInputLayout.error = getString(R.string.wrong_name_length)
            }
            Login.Error.NAME_REGEX -> {
                usernameTextInputLayout.error = getString(R.string.wrong_name)
            }
            Login.Error.WRONG_NAME -> {
                errorTextView.text = getString(R.string.login_error)
                errorTextView.visibility = VISIBLE
            }
            else -> throw IllegalStateException("IllegalStateException")
        }
    }

    private fun showInvalidPasswordMessage(passwordError: Password.Error) {
        hideAllErrors()
        when(passwordError) {
            Password.Error.INVALID_PASSWORD -> {
                passwordTextInputLayout.error = getString(R.string.password_invalid)
            }
            Password.Error.WRONG_PASSWORD -> {
                errorTextView.text = getString(R.string.password_error)
                errorTextView.visibility = VISIBLE
            }
            else -> throw IllegalStateException("IllegalStateException")
        }
    }

    private fun checkCredentials() {
        viewModel.checkUserCredentials(
            Credentials(
                usernameTextInputLayout.editText?.text.toString(),
                passwordTextInputLayout.editText?.text.toString()
            )
        )
    }

    private fun hideAllErrors() {
        usernameTextInputLayout.error = ""
        passwordTextInputLayout.error = ""
    }

    private fun startTransactionListActivity() {
        requireActivity().startActivity(
            Intent(requireActivity(), TransactionListActivity::class.java)
        )
        requireActivity().finish()
    }

    companion object {

        fun newInstance() = SignInFragment()
    }
}
package com.example.financial.ui.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

abstract class BaseAdapter<T> : RecyclerView.Adapter<BaseAdapter<T>.BaseViewHolder<T>>() {

    private val items = mutableListOf<T>()

    fun setElements(elements: Collection<T>) {
        items.clear()
        items.addAll(elements)
        notifyDataSetChanged()
    }

    protected fun removeElement(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    fun getItem(position: Int): T = items[position]

    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T> {
        val inflater = LayoutInflater.from(parent.context)
        val layoutId = getLayoutId(viewType)
        val view = inflater.inflate(layoutId, parent, false)
        return getViewHolder(view, viewType)
    }

    final override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bind(items[position])
    }

    final override fun getItemCount(): Int {
        return items.size
    }

    @LayoutRes protected abstract fun getLayoutId(viewType: Int): Int

    abstract fun getViewHolder(view: View, viewType: Int): BaseViewHolder<T>

    abstract inner class BaseViewHolder<T>(
        override val containerView: View
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        abstract fun bind(data: T)
    }
}
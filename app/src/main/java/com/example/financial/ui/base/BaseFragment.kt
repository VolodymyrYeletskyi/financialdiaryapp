package com.example.financial.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.example.financial.util.context.getViewModelProvider
import com.example.financial.ui.dialog.ProgressDialogFragment
import com.example.financial.util.livedata.observe
import java.lang.reflect.ParameterizedType

abstract class BaseFragment<VM: BaseViewModel>(
    @LayoutRes layoutId: Int
) : Fragment(layoutId) {

    protected val viewModel: VM by lazy {
        requireContext().getViewModelProvider(this)
            .get((javaClass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<VM>)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        subscribe()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    fun loadFragment(@IdRes fragmentId: Int, fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(fragmentId, fragment)
            addToBackStack(null)
        }.commit()
    }

    protected fun showProgressBar() {
        ProgressDialogFragment.show(childFragmentManager)
    }

    protected fun hideProgressBar() {
        ProgressDialogFragment.dismiss(childFragmentManager)
    }

    @CallSuper protected open fun subscribe() {
        viewModel.isProgressBarShowedData.observe(this) {
            if(it) showProgressBar() else hideProgressBar()
        }
    }
}
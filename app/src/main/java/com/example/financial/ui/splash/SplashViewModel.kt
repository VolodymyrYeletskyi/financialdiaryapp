package com.example.financial.ui.splash

import androidx.lifecycle.LiveData
import com.example.financial.data.repository.UserRepository
import com.example.financial.ui.base.BaseViewModel
import com.example.financial.util.livedata.SingleLiveEvent
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel(private val userRepository: UserRepository) : BaseViewModel() {

    private val _userAuthorizedEvent = SingleLiveEvent<Unit>()
    val userAuthorizedEvent: LiveData<Unit> = _userAuthorizedEvent

    private val _userNotAuthorizedEvent = SingleLiveEvent<Unit>()
    val userNotAuthorizedEvent: LiveData<Unit> = _userNotAuthorizedEvent

    init {
        loadUser()
    }

    private fun loadUser() {
        scope.launch {
            val delayJob = scope.launch {
                delay(delayTime)
            }
            val isUserAuthorizedJob = scope.async {
                userRepository.isAuthorized()
            }
            delayJob.join()
            if(isUserAuthorizedJob.await()) {
                _userAuthorizedEvent.postValue(Unit)
            }
            else {
                _userNotAuthorizedEvent.postValue(Unit)
            }
        }
    }

    companion object {

        private const val delayTime = 2000L
    }
}
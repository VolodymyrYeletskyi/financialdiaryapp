package com.example.financial.ui.base

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.financial.util.livedata.SingleLiveEvent
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.plus

abstract class BaseViewModel: ViewModel() {

    private val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG, "You got an exception", exception)
    }

    val scope = viewModelScope + handler

    private val _isProgressBarShowedData = MutableLiveData<Boolean>()
    val isProgressBarShowedData: LiveData<Boolean> = _isProgressBarShowedData

    protected fun showProgressBar() {
        _isProgressBarShowedData.postValue(true)
    }

    protected fun hideProgressBar() {
        _isProgressBarShowedData.postValue(false)
    }

    companion object {

        private val TAG = CoroutineExceptionHandler::class.java.canonicalName
    }
}
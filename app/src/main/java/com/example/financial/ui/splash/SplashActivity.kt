package com.example.financial.ui.splash

import android.content.Intent
import android.os.Bundle
import android.view.animation.AnimationUtils
import com.example.financial.R
import com.example.financial.ui.authorization.AuthActivity
import com.example.financial.ui.base.BaseActivity
import com.example.financial.ui.transaction.TransactionListActivity
import com.example.financial.util.livedata.observe
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity<SplashViewModel>(R.layout.activity_splash) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageView.animation = AnimationUtils.loadAnimation(this, R.anim.fadein)
    }

    override fun subscribe() {
        super.subscribe()
        viewModel.userNotAuthorizedEvent.observe(this) {
            startActivity(AuthActivity::class.java)
        }
        viewModel.userAuthorizedEvent.observe(this) {
            startActivity(TransactionListActivity::class.java)
        }
    }

    private fun startActivity(cls: Class<*>) {
        startActivity(Intent(this, cls))
        finish()
    }
}
package com.example.financial.ui.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.financial.R

class ProgressDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.fragment_progress_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    companion object {

        private val TAG = ProgressDialogFragment::class.java.canonicalName

        private fun newInstance() = ProgressDialogFragment()

        fun show(fragmentManager: FragmentManager) {
            var progressDialogFragment = fragmentManager.findFragmentByTag(TAG)

            if (progressDialogFragment == null) {
                progressDialogFragment = newInstance()
                progressDialogFragment.show(fragmentManager, TAG)
            }
        }

        fun dismiss(fragmentManager: FragmentManager) {
            (fragmentManager.findFragmentByTag(TAG) as? ProgressDialogFragment)?.dismiss()
        }
    }
}
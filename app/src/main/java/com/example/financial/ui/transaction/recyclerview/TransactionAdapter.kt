package com.example.financial.ui.transaction.recyclerview

import android.view.View
import androidx.core.content.ContextCompat
import com.example.financial.Application
import com.example.financial.R
import com.example.financial.data.entity.Transaction
import com.example.financial.ui.base.BaseAdapter
import com.example.financial.util.enum.TransactionType
import kotlinx.android.synthetic.main.transaction_item.view.*

class TransactionAdapter(val clickListener: (Transaction) -> Unit) :
    BaseAdapter<Transaction>()
{

    fun removeTransaction(position: Int) {
        removeElement(position)
    }

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.transaction_item
    }

    override fun getViewHolder(view: View, viewType: Int): BaseViewHolder<Transaction> {
        return TransactionViewHolder(view)
    }

    inner class TransactionViewHolder(override val containerView: View) :
        BaseViewHolder<Transaction>(containerView)
    {

        override fun bind(data: Transaction) {
            containerView.setOnClickListener { clickListener(data) }
            containerView.nameTextView.text = data.name
            if (data.type == TransactionType.INCOME) {
                containerView.amountTextView.setTextColor(
                    ContextCompat.getColor(containerView.context, R.color.colorGreen)
                )
                containerView.amountTextView.text = containerView.resources.getString(
                    R.string.plus, data.amount.toString()
                )
            } else {
                containerView.amountTextView.setTextColor(
                    ContextCompat.getColor(containerView.context, R.color.colorAlmostRed)
                )
                containerView.amountTextView.text = containerView.resources.getString(
                    R.string.minus, data.amount.toString()
                )
            }
        }
    }
}
package com.example.financial.ui.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.example.financial.util.context.getViewModelProvider
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<VM: BaseViewModel>(
    @LayoutRes layoutId: Int
) : AppCompatActivity(layoutId) {

    protected val viewModel: VM by lazy {
        getViewModelProvider(this)
            .get((javaClass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<VM>)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subscribe()
    }

    @CallSuper protected open fun subscribe() {
    }
}
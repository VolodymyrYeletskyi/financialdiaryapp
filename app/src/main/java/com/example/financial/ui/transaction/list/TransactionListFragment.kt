package com.example.financial.ui.transaction.list

import android.os.Bundle
import android.view.View
import android.view.View.VISIBLE
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.financial.R
import com.example.financial.data.entity.Transaction
import com.example.financial.util.livedata.observe
import com.example.financial.ui.base.BaseFragment
import com.example.financial.ui.transaction.new_transaction.NewTransactionFragment
import com.example.financial.ui.transaction.recyclerview.SwipeTouchHelper
import com.example.financial.ui.transaction.recyclerview.TransactionAdapter
import com.example.financial.util.enum.TransactionType
import kotlinx.android.synthetic.main.activity_transaction_list.*
import kotlinx.android.synthetic.main.fragment_transaction_list.*

class TransactionListFragment :
    BaseFragment<TransactionListViewModel>(R.layout.fragment_transaction_list)
{

    private val transactionAdapter =
        TransactionAdapter { itemClicked: Transaction ->
            (clickTransaction(itemClicked))
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getTransactions(requireFilter())

        requireActivity().floatingActionButton.visibility = VISIBLE

        SwipeTouchHelper(::onSwipe)
            .attachToRecyclerView(transactionRecyclerView)
        val divider = DividerItemDecoration(context, RecyclerView.VERTICAL)

        transactionRecyclerView.addItemDecoration(divider)
        transactionRecyclerView.adapter = transactionAdapter
    }

    private fun clickTransaction(transaction: Transaction) {
        TODO("Show fragment with detailed information")
    }

    override fun subscribe() {
        super.subscribe()
        viewModel.getTransactionsEvent.observe(viewLifecycleOwner) {
            transactionAdapter.setElements(it)
        }
    }

    fun getTitleString(): Int {
        return when(requireFilter()) {
            TransactionType.INCOME -> R.string.income
            TransactionType.ALL -> R.string.all
            TransactionType.EXPENSE -> R.string.expense
        }
    }

    private fun requireFilter() = requireArguments().get(KEY_FILTER) as TransactionType

    private fun onSwipe(viewHolder: RecyclerView.ViewHolder, moveDirection: Int) {
        val transaction = transactionAdapter.getItem(viewHolder.adapterPosition)

        transactionAdapter.removeTransaction(viewHolder.adapterPosition)
        viewModel.removeTransaction(transaction)
    }

    companion object {

        private val KEY_FILTER = TransactionType::class.java.canonicalName.toString()

        fun newInstance(filter: TransactionType): TransactionListFragment {
            return TransactionListFragment()
                .apply {
                arguments = bundleOf(KEY_FILTER to filter)
            }
        }
    }
}
package com.example.financial.ui.authorization

import android.os.Bundle
import com.example.financial.R
import com.example.financial.ui.base.BaseActivity

class AuthActivity : BaseActivity<SignUpViewModel>(R.layout.activity_auth) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            showSignInFragment()
        }
    }
    private fun showSignInFragment() {
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_authorization, SignInFragment.newInstance())
            .commit()
    }
}
package com.example.financial.ui.transaction.list

import android.os.Bundle
import android.view.View
import com.example.financial.R
import com.example.financial.ui.base.BaseFragment
import com.example.financial.ui.transaction.viewpager.ViewPagerAdapter
import com.example.financial.util.enum.TransactionType
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_tab.filterTabLayout
import kotlinx.android.synthetic.main.fragment_tab.viewPager

class TabFragment : BaseFragment<TransactionListViewModel>(R.layout.fragment_tab) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {
            setupViewPager(DEFAULT_TAB)
        } else {
            val selectedTab = savedInstanceState.getInt(KEY_TAB_INDEX,
                DEFAULT_TAB)
            setupViewPager(selectedTab)
        }
    }

    private fun setupViewPager(selectedTab: Int) {
        val pagerAdapter =
            ViewPagerAdapter(this)
        viewPager.adapter = pagerAdapter
        viewPager.isUserInputEnabled = false

        TabLayoutMediator(filterTabLayout, viewPager) { tab, position ->
            tab.text = getString(pagerAdapter.getTabTitle(position))
        }.attach()

        viewPager.setCurrentItem(selectedTab, false)
    }

    companion object {

        private const val DEFAULT_TAB = 0

        private val KEY_TAB_INDEX = ViewPagerAdapter::class.java.canonicalName

        fun newInstance() = TabFragment()
    }
}
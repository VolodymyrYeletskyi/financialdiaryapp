package com.example.financial.ui.transaction.viewpager

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.financial.ui.transaction.list.TransactionListFragment
import com.example.financial.util.enum.TransactionType

class ViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private val transactionFragments = mutableListOf<TransactionListFragment>().apply {
        add(TransactionListFragment.newInstance(
            TransactionType.ALL))
        add(TransactionListFragment.newInstance(
            TransactionType.INCOME))
        add(TransactionListFragment.newInstance(
            TransactionType.EXPENSE))
    }

    override fun createFragment(position: Int): Fragment {
        return transactionFragments[position]
    }

    override fun getItemCount(): Int = transactionFragments.size

    fun getTabTitle(position: Int): Int = transactionFragments[position].getTitleString()
}
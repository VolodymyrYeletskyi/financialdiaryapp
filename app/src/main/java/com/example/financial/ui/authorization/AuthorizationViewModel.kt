package com.example.financial.ui.authorization

import androidx.lifecycle.LiveData
import com.example.financial.data.entity.Credentials
import com.example.financial.data.repository.UserRepository
import com.example.financial.ui.base.BaseViewModel
import com.example.financial.util.livedata.SingleLiveEvent
import com.example.financial.util.validation.Login
import com.example.financial.util.validation.Password
import kotlinx.coroutines.launch

class AuthorizationViewModel(private val userRepository: UserRepository) : BaseViewModel() {

    private val _validationSuccessfulEvent = SingleLiveEvent<Unit>()
    val validationSuccessfulEvent: LiveData<Unit> = _validationSuccessfulEvent

    private val _invalidLoginEvent = SingleLiveEvent<Login.Error>()
    val invalidLoginEvent: LiveData<Login.Error> = _invalidLoginEvent

    private val _invalidPasswordEvent = SingleLiveEvent<Password.Error>()
    val invalidPasswordEvent: LiveData<Password.Error> = _invalidPasswordEvent

    private val _userAuthorizedEvent = SingleLiveEvent<Unit>()
    val userAuthorizedEvent: LiveData<Unit> = _userAuthorizedEvent

    fun checkUserCredentials(credentials: Credentials) = scope.launch {
        showProgressBar()

        val validationLogin = Login.validateLogin(credentials.name)
        val validationPassword = Password.validatePassword(credentials.password)

        if (validationLogin == Login.Error.SUCCESS &&
            validationPassword == Password.Error.SUCCESS
        ) {
            _validationSuccessfulEvent.postValue(Unit)
            signIn(credentials)
        } else {
            if (validationLogin != Login.Error.SUCCESS) {
                _invalidLoginEvent.postValue(validationLogin)
            }

            if (validationPassword != Password.Error.SUCCESS) {
                _invalidPasswordEvent.postValue(validationPassword)
            }
        }

        hideProgressBar()
    }

    private suspend fun signIn(credentials: Credentials) {
        val result = userRepository.signIn(credentials)
        if (result.isSuccess) {
            _userAuthorizedEvent.postValue(Unit)
        }
        else {
            when(result.exceptionOrNull()?.message) {
                "Wrong username" -> {
                    _invalidLoginEvent.postValue(Login.Error.WRONG_NAME)
                }
                "Wrong password" -> {
                    _invalidPasswordEvent.postValue(Password.Error.WRONG_PASSWORD)
                }
            }
        }
    }
}
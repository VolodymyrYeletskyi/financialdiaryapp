package com.example.financial.ui.transaction.new_transaction

import android.os.Bundle
import android.view.View
import android.view.View.INVISIBLE
import android.widget.RadioButton
import android.widget.Toast
import com.example.financial.R
import com.example.financial.util.livedata.observe
import com.example.financial.ui.base.BaseFragment
import com.example.financial.ui.transaction.list.TabFragment
import com.example.financial.util.enum.TransactionType
import com.example.financial.util.validation.NewTransaction
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_transaction_list.*
import kotlinx.android.synthetic.main.fragment_new_transaction.*

class NewTransactionFragment : BaseFragment<NewTransactionViewModel>(
    R.layout.fragment_new_transaction
) {

    private var transactionType =  TransactionType.INCOME

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().floatingActionButton.visibility = INVISIBLE
        radioButtonIncome.isChecked = true

        if (savedInstanceState != null) {
            radioButtonIncome.isChecked = savedInstanceState.getBoolean("income")
            radioButtonExpense.isChecked = savedInstanceState.getBoolean("expense")

            textInputLayoutName.editText?.setText(savedInstanceState.getString("name"))
            textInputLayoutAmount.editText?.setText(savedInstanceState.getString("amount"))
            textInputLayoutDescription.editText?.setText(savedInstanceState.getString("description"))
        }

        setupTextInputLayout(textInputLayoutName)
        setupTextInputLayout(textInputLayoutAmount)

        radioButtonIncome.setOnClickListener {
            chooseTransactionType(it)
        }
        radioButtonExpense.setOnClickListener {
            chooseTransactionType(it)
        }
        buttonSave.setOnClickListener {
            saveTransaction()
        }
        buttonBack.setOnClickListener {
            goBack()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if(radioButtonIncome.isChecked) outState.putBoolean("income", true)
        else outState.putBoolean("income", false)

        if(radioButtonExpense.isChecked) outState.putBoolean("expense", true)
        else outState.putBoolean("expense", false)

        outState.putString("name", textInputLayoutName.editText?.text.toString())
        outState.putString("amount", textInputLayoutAmount.editText?.text.toString())
        outState.putString("description", textInputLayoutDescription.editText?.text.toString())
    }

    override fun subscribe() {
        super.subscribe()
        viewModel.rightTransactionEvent.observe(viewLifecycleOwner) {
            hideAllErrorsAndShowToast()
        }
        viewModel.wrongTransactionEvent.observe(viewLifecycleOwner) {
            showError(it)
        }
    }

    private fun chooseTransactionType(view: View) {
        val radioButton = view as RadioButton
        when(radioButton.id) {
            R.id.radioButtonIncome -> transactionType = TransactionType.INCOME
            R.id.radioButtonExpense -> transactionType = TransactionType.EXPENSE
        }
    }

    private fun saveTransaction() {
        viewModel.validateTransaction(
            textInputLayoutName.editText?.text.toString(),
            transactionType,
            textInputLayoutAmount.editText?.text.toString(),
            textInputLayoutDescription.editText?.text.toString()
        )
    }

    private fun goBack() {
        requireActivity()
            .supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment, TabFragment.newInstance())
            .commit()
    }

    private fun showError(error: NewTransaction.Error) {
        when(error) {
            NewTransaction.Error.WRONG_NAME -> {
                textInputLayoutName.error = getString(R.string.wrong_transaction_name)
            }
            NewTransaction.Error.WRONG_AMOUNT -> {
                textInputLayoutAmount.error = getString(R.string.wrong_transaction_amount)
            }
            else -> throw IllegalStateException("IllegalStateException")
        }
    }

    private fun hideAllErrorsAndShowToast() {
        textInputLayoutName.error = ""
        textInputLayoutAmount.error = ""
        Toast.makeText(context, R.string.success, Toast.LENGTH_SHORT).show()
    }

    private fun setupTextInputLayout(textInputLayout: TextInputLayout?) {
        textInputLayout?.editText?.onFocusChangeListener =
            View.OnFocusChangeListener {_, hasFocus ->  if(hasFocus) textInputLayout?.error = ""}
    }

    companion object {

        fun newInstance() = NewTransactionFragment()
    }
}